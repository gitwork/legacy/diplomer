# pylint: disable=C0301,W0311,R0914
"""Validation module
Check issue to correct filling"""
from export_from_gitlab.correct_filled_issues import correct_filled_issue


GLOBAL_FIELD_NUMBERS_IN_KURS = 8
GLOBAL_FIELD_NUMBERS_IN_DIPLOM = 5
GLOBAL_NO_FOUND = -1
GLOBAL_MIN_NUM_OF_PARAGRAPH_KURS = 2
GLOBAL_MIN_NUM_OF_PARAGRAPH_DIPLOM = 5
GLOBAL_TABS = 2
GLOBAL_SPACE_AFTER_COLON = 1
GLOBAL_SPACE_BEFORE_TEXT = 2


class ValidationModule:
    """This class contain all valid functions and check your issues for correct filling"""

    def check_json_valid(self, json_response):
        """Check issue(in json format) in all valid functions
            Args:
                json_response(our issue)
            Returns:
                The answer after passing all valid functions
                If all good return string: "Validation passed good"
                Otherwise return response with code 400 problem description
        """
        incorrect_issues_dict = {
            'incorrect_issues': [],
            'incorrect_issues_id': []
        }

        validation_answer_1 = self.valid_issue_to_correct_colon_in_title(json_response)
        validation_answer_2 = self.valid_issue_to_correct_num_of_colon_in_title(json_response)
        validation_answer_3 = self.valid_issue_to_correct_whitespace_in_title(json_response)
        validation_answer_4 = self.valid_issue_to_description_existance(json_response)
        validation_answer_5 = self.valid_issue_kurs_to_correct_separator_in_description(json_response)
        validation_answer_6 = self.valid_issue_diplom_to_correct_separator_in_description(json_response)
        validation_answer_7 = self.valid_issue_to_correct_stars_in_description(json_response)
        validation_answer_8 = self.valid_issue_to_correct_symbol_in_description_field(json_response)
        validation_answer_9 = self.valid_issue_to_correct_colon_in_description(json_response)
        validation_answer_10 = self.valid_issue_to_correct_num_of_colon_in_description(json_response)
        validation_answer_11 = self.valid_issue_to_correct_whitespace_in_description(json_response)
        validation_answer_12 = self.valid_issue_diplom_to_correct_paragraph_in_description(json_response)
        validation_answer_13 = self.valid_issue_kurs_to_correct_paragraph_in_description(json_response)
        #validation_answer_14 = self.valid_issue_to_correct_whitespaces_after_description_field(json_response)

        # validation_answer_list = [validation_answer_1,
        #                           validation_answer_2,
        #                           validation_answer_3,
        #                           validation_answer_4,
        #                           validation_answer_5,
        #                           validation_answer_6,
        #                           validation_answer_7,
        #                           validation_answer_8,
        #                           validation_answer_9,
        #                           validation_answer_10,
        #                           validation_answer_11,
        #                           validation_answer_12,
        #                           validation_answer_13,
        #                           validation_answer_14
        #                           ]

        validation_answer_list = [validation_answer_1,
                                  validation_answer_2,
                                  validation_answer_3,
                                  validation_answer_4,
                                  validation_answer_5,
                                  validation_answer_6,
                                  validation_answer_7,
                                  validation_answer_8,
                                  validation_answer_9,
                                  validation_answer_10,
                                  validation_answer_11,
                                  validation_answer_12,
                                  validation_answer_13,

                                  ]
        for answer in validation_answer_list:
            if len(answer['bad_issues']) != 0:
                for incorrect_issue in answer['bad_issues']:
                    incorrect_issues_dict['incorrect_issues'].append(incorrect_issue)
                    incorrect_issues_dict['incorrect_issues_id'].append(incorrect_issue['id'])

        return incorrect_issues_dict

    @classmethod
    def valid_issue_to_correct_num_of_colon_in_title(cls, json_response):
        """Check issue(in json format) to correct number of colon in issue title
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        incorrect_issues = {'bad_issues': []}
        for body in json_response:
            if body['title'].count(':') != 1:
                error_message = "Probably you have a lot of colon in: "
                what_to_do_message = " You should fill this field like this "
                error_content = error_message + str(body['title']) + \
                                what_to_do_message + \
                                str(correct_filled_issue['correct_filled_title'])
                bad_issue_dict = {'id': body['iid'],
                                  'error': error_content,
                                  'content': body['description']}
                incorrect_issues['bad_issues'].append(bad_issue_dict)
        return incorrect_issues

    @classmethod
    def valid_issue_to_correct_colon_in_title(cls, json_response):
        """Check issue(in json format) to correct filling colons in issue title
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        incorrect_issues = {'bad_issues': []}
        rus_alpha = "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя"
        for body in json_response:
            if body['title'].find(':') == GLOBAL_NO_FOUND \
                    or body['title'].startswith(tuple(rus_alpha)) is not True:
                error_message = "Probably you haven`t got colon in: "
                what_to_do_message = " You should fill this field like this "
                error_content = error_message + str(body['title']) + \
                                   what_to_do_message + \
                                   str(correct_filled_issue['correct_filled_title'])
                bad_issue_dict = {'id': body['iid'],
                                  'error': error_content,
                                  'content': body['description']}
                incorrect_issues['bad_issues'].append(bad_issue_dict)
        return incorrect_issues

    @classmethod
    def valid_issue_to_correct_colon_in_description(cls, json_response):
        """Check issue(in json format) to correct filling colons in issue description
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        good_separator = '---'
        incorrect_issues = {'bad_issues': []}

        for body in json_response:
            description_issue = body['description']
            issues_description_list = description_issue.split(good_separator)

            for description_field in issues_description_list[:-1]:
                if description_field.find(':') == GLOBAL_NO_FOUND:
                    error_message = "Probably you haven`t got colon: "
                    what_to_do_message = " You should fill this field like this "
                    error_content = error_message + \
                                       str(description_field) + " in issue: " + str(body['title']) + \
                                       what_to_do_message + \
                                       str(correct_filled_issue['correct_filled_description'])
                    bad_issue_dict = {
                        'id': body['iid'],
                        'error': error_content,
                        'content': body['description'],
                    }
                    incorrect_issues['bad_issues'].append(bad_issue_dict)
        return incorrect_issues

    @classmethod
    def valid_issue_to_correct_num_of_colon_in_description(cls, json_response):
        """Check issue(in json format) to correct num colons in issue description
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        good_separator = '---'
        incorrect_issues = {'bad_issues': []}

        for body in json_response:
            description_issue = body['description']
            issues_description_list = description_issue.split(good_separator)

            for description_field in issues_description_list[:-1]:
                if description_field.count(':') != 1:
                    error_message = "Probably you have got a lot of colon: "
                    what_to_do_message = " You should fill this field like this "
                    error_content = error_message + \
                                       str(description_field) + " in issue: " + str(body['title']) + \
                                       what_to_do_message + \
                                       str(correct_filled_issue['correct_filled_description'])
                    bad_issue_dict = {
                        'id': body['iid'],
                        'error': error_content,
                        'content': body['description'],
                    }
                    incorrect_issues['bad_issues'].append(bad_issue_dict)
        return incorrect_issues

    @classmethod
    def valid_issue_kurs_to_correct_separator_in_description(cls, json_response):
        """Check issue(in json format) to correct filling separators in kurs issue description
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        incorrect_issues = {'bad_issues': []}
        good_separator = '---'

        for body in json_response:
            description_issue = body['description']
            kurs_theme_title = "**Тема курсовой работы семестра:**"
            if description_issue.startswith(kurs_theme_title):
                issues_description_list = description_issue.split(good_separator)
                if len(issues_description_list) != GLOBAL_FIELD_NUMBERS_IN_KURS:
                    error_message = "Probably you have incorrect separators in issue: "
                    what_to_do_message = " Correct separator is  "
                    error_content = error_message + str(body['title']) + \
                                       what_to_do_message + \
                                       str(correct_filled_issue['correct_filled_separator'])
                    bad_issue_dict = {
                        'id': body['iid'],
                        'error': error_content,
                        'content': body['description'],
                    }
                    incorrect_issues['bad_issues'].append(bad_issue_dict)
        return incorrect_issues

    @classmethod
    def valid_issue_diplom_to_correct_separator_in_description(cls, json_response):
        """Check issue(in json format) to correct filling separators in diplom issue description
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        incorrect_issues = {'bad_issues': []}
        good_separator = '---'

        for body in json_response:
            description_issue = body['description']
            work_theme = "**Тема работы:**"
            if description_issue.startswith(work_theme):
                issues_description_list = description_issue.split(good_separator)
                if len(issues_description_list) != GLOBAL_FIELD_NUMBERS_IN_DIPLOM:
                    error_message = "Probably you have incorrect separators in issue: "
                    what_to_do_message = " Correct separator is  "
                    error_content = error_message + str(body['title']) + \
                                       what_to_do_message + \
                                       str(correct_filled_issue['correct_filled_separator'])
                    bad_issue_dict = {
                        'id': body['iid'],
                        'error': error_content,
                        'content': body['description'],
                    }
                    incorrect_issues['bad_issues'].append(bad_issue_dict)
        return incorrect_issues

    @classmethod
    def valid_issue_kurs_to_correct_paragraph_in_description(cls, json_response):
        """Check issue(in json format) to correct filling paragraphs in kurs issue description
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        good_separator = '---'
        main_questions = 'Основные вопросы и задачи'
        main_questions_kurs_autumn = 'Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра'
        main_questions_kurs_spring = 'Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра'
        incorrect_issues = {'bad_issues': []}

        for body in json_response:
            description_issue = body['description']

            issues_description_list = description_issue.split(good_separator)
            parsed_issues_dict = {}
            for description_field in issues_description_list[:-1]:

                issues_replaced_list = description_field.replace('**', '').strip("\n").strip("\r\n")
                if main_questions in description_field:
                    parsed_issues_dict.update({issues_replaced_list[:issues_replaced_list.find(':')]: issues_replaced_list[issues_replaced_list.find(':') + GLOBAL_TABS:].split('\n')})

                    if main_questions_kurs_autumn in parsed_issues_dict and \
                            len(parsed_issues_dict[main_questions_kurs_autumn])\
                            < GLOBAL_MIN_NUM_OF_PARAGRAPH_KURS:
                        error_message = "Not enough paragrpah in issue description: "
                        where_is_message = "Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра in issue: "
                        what_to_do_message = " You should have at least "
                        error_content = error_message + where_is_message + \
                                                       str(body['title']) + what_to_do_message + str(correct_filled_issue['correct_min_num_of_paragraph_kurs'])
                        bad_issue_dict = {
                            'id': body['iid'],
                            'error': error_content,
                            'content': body['description'],
                        }
                        incorrect_issues['bad_issues'].append(bad_issue_dict)

                    if main_questions_kurs_spring in parsed_issues_dict and \
                            len(parsed_issues_dict[main_questions_kurs_spring])\
                            < GLOBAL_MIN_NUM_OF_PARAGRAPH_KURS:
                        error_message = "Not enough paragrpah in issue description: "
                        where_is_message = "Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра in issue: "
                        what_to_do_message = " You should have at least "
                        error_content = error_message + where_is_message\
                                           + str(body['title']) + what_to_do_message + str(correct_filled_issue['correct_min_num_of_paragraph_kurs'])

                        bad_issue_dict = {
                            'id': body['iid'],
                            'error': error_content,
                            'content': body['description'],
                        }
                        incorrect_issues['bad_issues'].append(bad_issue_dict)

                    parsed_issues_dict.clear()

        return incorrect_issues

    @classmethod
    def valid_issue_diplom_to_correct_paragraph_in_description(cls, json_response):
        """Check issue(in json format) to correct filling paragraphs in diplom issue description
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        incorrect_issues = {'bad_issues': []}
        good_separator = '---'
        main_questions = 'Основные вопросы и задачи'
        main_questions_diplom = "Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы"

        for body in json_response:
            description_issue = body['description']

            issues_description_list = description_issue.split(good_separator)
            parsed_issues_dict = {}

            for description_field in issues_description_list[:-1]:
                issues_replaced_list = description_field.replace('**', '').strip("\n").strip("\r\n")
                if main_questions in description_field:
                    parsed_issues_dict.update({issues_replaced_list[:issues_replaced_list.find(':')]:
                                                   issues_replaced_list[issues_replaced_list.find(':') + GLOBAL_TABS:].split('\n')})

                    if parsed_issues_dict[issues_replaced_list[:issues_replaced_list.find(':')]][0] == "":
                        parsed_issues_dict[issues_replaced_list[:issues_replaced_list.find(':')]].remove("")

                    if main_questions_diplom in parsed_issues_dict and \
                            len(parsed_issues_dict[main_questions_diplom])\
                            < GLOBAL_MIN_NUM_OF_PARAGRAPH_DIPLOM:
                        error_message = "Not enough paragrpah in issue description: "
                        where_is_message = "Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы in issue "
                        what_to_do_message = " You should have at least "
                        error_content = error_message + where_is_message\
                                           + str(body['title']) + what_to_do_message + str(correct_filled_issue['correct_min_num_of_paragraph_diplom'])
                        bad_issue_dict = {
                            'id': body['iid'],
                            'error': error_content,
                            'content': body['description'],
                        }
                        incorrect_issues['bad_issues'].append(bad_issue_dict)
                    parsed_issues_dict.clear()

        return incorrect_issues

    @classmethod
    def valid_issue_to_correct_symbol_in_description_field(cls, json_response):
        """Check issue(in json format) to correct filling symbols in issue description
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        rus_alpha = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNMАаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя.1234567890 "
        good_separator = '---'
        incorrect_issues = {'bad_issues': []}

        for body in json_response:
            description_issue = body['description']

            issues_description_list = description_issue.split(good_separator)

            for description_field in issues_description_list[:-1]:
                issues_replaced_list = description_field.replace('**', '').strip("\n").strip("\r\n")

                if issues_replaced_list[:issues_replaced_list.find(':')].startswith(tuple(rus_alpha)) and \
                        issues_replaced_list[:issues_replaced_list.find(':')].endswith(tuple(rus_alpha)) and \
                        issues_replaced_list[issues_replaced_list.find(':') + GLOBAL_TABS:].startswith(tuple(rus_alpha)) and \
                        issues_replaced_list[issues_replaced_list.find(':') + GLOBAL_TABS:].endswith(tuple(rus_alpha)):
                        pass
                else:
                    error_message = "Probably you have incorrect symbols in description: "
                    what_to_do_message = " You should fill this field like this "
                    error_content = error_message \
                                       + issues_replaced_list[:issues_replaced_list.find(':')] + ' or ' + \
                                       str(issues_replaced_list[issues_replaced_list.find(':') + GLOBAL_TABS:]) \
                                       + " in issue: " + str(body['title']) + what_to_do_message +\
                                       str(correct_filled_issue['correct_filled_description'])
                    bad_issue_dict = {
                        'id': body['iid'],
                        'error': error_content,
                        'content': body['description'],
                    }
                    incorrect_issues['bad_issues'].append(bad_issue_dict)

        return incorrect_issues

    @classmethod
    def valid_issue_to_correct_whitespace_in_title(cls, json_response):
        """Check issue(in json format) to correct filling whitespace in issue title
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        incorrect_issues = {'bad_issues': []}

        for body in json_response:
            issues_dict = {}

            if body['title'].endswith(":") or body['title'][body['title'].find(':') + GLOBAL_SPACE_AFTER_COLON] != " ":

                error_message = "Probably you haven`t got whitespace in: "
                what_to_do_message = " You should fill this field like this "
                error_content = error_message + str(body['title']) + \
                                   what_to_do_message + \
                                   str(correct_filled_issue['correct_filled_title'])
                bad_issue_dict = {
                    'id': body['iid'],
                    'error': error_content,
                    'content': body['description'],
                }
                incorrect_issues['bad_issues'].append(bad_issue_dict)

            issues_dict['theme'] = body['title'][body['title'].find(':') + GLOBAL_TABS:]

            if issues_dict['theme'].startswith(" ") or issues_dict['theme'].endswith(" "):
                error_message = "Probably you have got extra whitespace in: "
                what_to_do_message = " You should fill this field like this "
                error_content = error_message + str(issues_dict['theme']) + \
                                   what_to_do_message + \
                                   str(correct_filled_issue['correct_filled_title'])
                bad_issue_dict = {
                    'id': body['iid'],
                    'error': error_content,
                    'content': body['description'],
                }
                incorrect_issues['bad_issues'].append(bad_issue_dict)
            issues_dict['title'] = body['title']
        return incorrect_issues

    @classmethod
    def valid_issue_to_correct_whitespace_in_description(cls, json_response):
        """Check issue(in json format) to correct filling whitespaces in issue description
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        incorrect_issues = {'bad_issues': []}
        good_separator = '---'

        for body in json_response:
            description_issue = body['description']

            issues_description_list = description_issue.split(good_separator)

            for description_field in issues_description_list[:-1]:
                issues_replaced_list = description_field.replace('**', '').strip("\n").strip("\r\n")

                if issues_replaced_list[issues_replaced_list.find(':') + GLOBAL_SPACE_AFTER_COLON] != " "\
                        and issues_replaced_list[issues_replaced_list.find(':') + GLOBAL_SPACE_AFTER_COLON] != "\n":
                    error_message = "Probably you haven`t got whitespace in: "
                    what_to_do_message = " You should fill this field like this "
                    where_is_message = " in issue: "
                    error_content = error_message + str(description_field) + \
                                       where_is_message + str(body['title']) + what_to_do_message +\
                                       str(correct_filled_issue['correct_filled_description'])
                    bad_issue_dict = {
                        'id': body['iid'],
                        'error': error_content,
                        'content': body['description'],
                    }
                    incorrect_issues['bad_issues'].append(bad_issue_dict)

                if issues_replaced_list[issues_replaced_list.find(':') + GLOBAL_SPACE_BEFORE_TEXT].startswith(" ") \
                        or issues_replaced_list[issues_replaced_list.find(':'):].endswith(" "):
                    error_message = "Probably you have got extra whitespace in: "
                    what_to_do_message = " You should fill this field like this "
                    where_is_message = " in issue: "
                    error_content = error_message + str(description_field) + \
                                       where_is_message + str(body['title']) + what_to_do_message +\
                                       str(correct_filled_issue['correct_filled_description'])
                    bad_issue_dict = {
                        'id': body['iid'],
                        'error': error_content,
                        'content': body['description'],
                    }
                    incorrect_issues['bad_issues'].append(bad_issue_dict)

        return incorrect_issues

    @classmethod
    def valid_issue_to_description_existance(cls, json_response):
        """Check issue(in json format) to empty or not issue description
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        incorrect_issues = {'bad_issues': []}
        for body in json_response:
            if body['description'] == '':
                error_message = "Probably you haven`t write anything in descrption issue: "
                error_content = error_message + str(body['title'])
                bad_issue_dict = {
                    'id': body['iid'],
                    'error': error_content,
                    'content': body['description'],
                }
                incorrect_issues['bad_issues'].append(bad_issue_dict)
        return incorrect_issues

    @classmethod
    def valid_issue_to_correct_stars_in_description(cls, json_response):
        """Check issue(in json format) to correct stars before and after description name
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        good_separator = '---'
        incorrect_issues = {'bad_issues': []}

        for body in json_response:
            description_issue = body['description']

            issues_description_list = description_issue.split(good_separator)

            for description_field in issues_description_list[:-1]:
                if description_field.strip("\n").startswith("**") is not True or description_field.count("**") != 2:
                    error_message = "Probably you haven`t got ** in description field: "
                    what_to_do_message = " You should fill this field like this "
                    error_content = error_message + str(body['title']) + \
                                    what_to_do_message + \
                                    str(correct_filled_issue['correct_filled_description'])
                    bad_issue_dict = {'id': body['iid'],
                                      'error': error_content,
                                      'content': body['description']}
                    incorrect_issues['bad_issues'].append(bad_issue_dict)
        return incorrect_issues

    @classmethod
    def valid_issue_to_correct_whitespaces_after_description_field(cls, json_response):
        """Check issue(git in json format) to correct whitespaces after description field
            Args:
                json_response(our issue)
            Returns:
                If issue is correct return good response with code 200
                Otherwise return response with code 400 problem description
        """
        good_separator = '---'
        incorrect_issues = {'bad_issues': []}

        for body in json_response:
            description_issue = body['description']

            issues_description_list = description_issue.split(good_separator)

            for description_field in issues_description_list[:-1]:
                slice_last_elems = description_field[-1:-4:-1]
                if description_field.endswith("\n\n") is not True or slice_last_elems.count("\n") != 2:
                    error_message = "Probably you haven`t got enough whitespaces after description field: "
                    what_to_do_message = " You should fill this field like this "
                    error_content = error_message + str(body['title']) + \
                                    what_to_do_message + \
                                    str(correct_filled_issue['correct_filled_description'])
                    bad_issue_dict = {'id': body['iid'],
                                      'error': error_content,
                                      'content': body['description']}
                    incorrect_issues['bad_issues'].append(bad_issue_dict)
        return incorrect_issues
