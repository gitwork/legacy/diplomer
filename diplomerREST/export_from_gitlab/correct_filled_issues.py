"""Contain examples of correct filled issues"""


correct_filled_issue = {
    'correct_filled_title': "Петров: модель земли под коралловым рифом",
    'correct_filled_separator': "---",
    'correct_filled_description': "**Тема работы:** Программное "
                                  "средство формирования расписания из "
                                  "тематических планов учебных дисциплин.",
    'correct_min_num_of_paragraph_kurs': 2,
    'correct_min_num_of_paragraph_diplom': 5,

}
