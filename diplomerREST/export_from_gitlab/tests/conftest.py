# pylint: disable=C0301,C0330,C0302
"""Fixtures for tests"""
from urllib.parse import urlencode
import pytest
import requests
from export_from_gitlab.validation_module import ValidationModule


DEFAULT_IP = '127.0.0.1:8000'


@pytest.fixture
def supply_arguments_to_test_good_issue_endpoint_connection():
    """Make correct /issue connection"""
    params = {
        'token': 'vQHqMeqB_4UoTy3iX_mx',
        'login': 'dull69',
        'project_name': 'diplomer',
        'labels': '2019-2020',
        'state': 'all',
        'teachers_name': ''
    }
    url = f'http://{DEFAULT_IP}/diplomer/api/v1/issues/issue/?{urlencode(params)}'
    response = requests.get(url)

    return response


@pytest.fixture
def supply_arguments_to_test_good_issue_endpoint_connection_with_teachers():
    """Make correct /issue connection"""
    params = {
        'token': 'vQHqMeqB_4UoTy3iX_mx',
        'login': 'dull69',
        'project_name': 'diplomer',
        'labels': '2019-2020',
        'state': 'opened',
        'teachers_name': 'Петров'
    }
    url = f'http://{DEFAULT_IP}/diplomer/api/v1/issues/issue/?{urlencode(params)}'
    response = requests.get(url)

    return response


@pytest.fixture
def supply_arguments_to_test_bad_issue_endpoint_connection():
    """Make correct /issue  bad connection"""
    params = {
        'token': 'vQHqMeqB_4UoTy3iX_x',
        'login': 'dull69',
        'project_name': 'diplomer',
        'labels': '2019-2020',
        'state': 'all',
        'teachers_name': ''
    }
    url = f'http://{DEFAULT_IP}/diplomer/api/v1/issues/issue/?{urlencode(params)}'
    response = requests.get(url)

    return response


@pytest.fixture
def supply_arguments_to_test_good_check_correct_endpoint_connection():
    """Make correct connection"""
    params = {
        'token': 'vQHqMeqB_4UoTy3iX_mx',
        'login': 'dull69',
        'project_name': 'diplomer',
        'labels': 'bad',
        'state': 'all',
        'teachers_name': ''
    }
    url = f'http://{DEFAULT_IP}/diplomer/api/v1/issues/check_correct/?{urlencode(params)}'
    response = requests.get(url)

    return response


@pytest.fixture
def supply_arguments_to_test_bad_check_correct_endpoint_connection():
    """Make correct connection"""
    params = {
        'token': 'vQHqMeqB_4Uo3iX_mx',
        'login': 'dull69',
        'project_name': 'diplomer',
        'labels': 'bad',
        'state': 'opened',
        'teachers_name': ''
    }
    url = f'http://{DEFAULT_IP}/diplomer/api/v1/issues/check_correct/?{urlencode(params)}'
    response = requests.get(url)

    return response


@pytest.fixture
def supply_arguments_to_test_connection_without_token():
    """Make connection without token in url"""
    params = {
        'login': 'dull69',
        'project_name': 'diplomer',
        'labels': 'bad',
        'state': 'opened',
        'teachers_name': ''
    }
    url = f'http://{DEFAULT_IP}/diplomer/api/v1/issues/check_correct/?{urlencode(params)}'
    response = requests.get(url)

    return response


@pytest.fixture
def supply_arguments_to_test_connection_without_labels():
    """Make connection without labels in url"""
    params = {
        'token': 'vQHqMeqB_4Uo3iX_mx',
        'login': 'dull69',
        'project_name': 'diplomer',
        'state': 'opened',
        'teachers_name': ''
    }
    url = f'http://{DEFAULT_IP}/diplomer/api/v1/issues/check_correct/?{urlencode(params)}'
    response = requests.get(url)

    return response


@pytest.fixture
def supply_json_to_test_correct_colon_title_incorrect_input():
    """Make incorrect json for tests colons in title"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': 'иванов модель кактуса',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n*Аннотация:* замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_return = test_check_valid.valid_issue_to_correct_colon_in_title(issues_json)

    return test_check_valid_return


@pytest.fixture
def supply_json_to_test_correct_colon_title_incorrect_input_one_word():
    """Make incorrect json with one word in title for tests colons in title"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': 'ивановк',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n*Аннотация:* замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_return = test_check_valid.valid_issue_to_correct_colon_in_title(issues_json)

    return test_check_valid_return


@pytest.fixture
def supply_json_to_test_correct_colon_title_incorrect_input_one_word():
    """Make incorrect json with one word in title for tests colons in title"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': 'ивановк',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n*Аннотация:* замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_return = test_check_valid.valid_issue_to_correct_colon_in_title(issues_json)

    return test_check_valid_return


@pytest.fixture
def supply_json_to_test_correct_num_of_colon_in_title_incorrect_input_double_colon():
    """Make incorrect json with one word in title for tests colons in title"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': 'иван:: при',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n*Аннотация:* замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_return = test_check_valid.valid_issue_to_correct_num_of_colon_in_title(issues_json)

    return test_check_valid_return


@pytest.fixture
def supply_json_to_test_correct_colon_title_incorrect_input_only_colon():
    """Make incorrect json with only colon in title for tests colons in title"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': ':',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n*Аннотация:* замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_return = test_check_valid.valid_issue_to_correct_colon_in_title(issues_json)

    return test_check_valid_return


@pytest.fixture
def supply_json_to_test_correct_colon_title_incorrect_input_startswith_no_letter():
    """Make incorrect json with only colon in title for tests colons in title"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': '>4#>?: &;',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n*Аннотация:* замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_return = test_check_valid.valid_issue_to_correct_colon_in_title(issues_json)

    return test_check_valid_return


@pytest.fixture
def supply_json_to_test_correct_colon_title_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests colons in title"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': 'иванов модель кактуса',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n*Аннотация:* замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
                   {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
                   'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                                  '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                  '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                  '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                                  '1. Изучить\n'
                                  '1. Исследовать\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                  '1. Разработать\n'
                                  '1. Предоставить\n\n---\n'
                                  '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
                   {'id': 32405282, 'iid': 2, 'project_id': 17692961,
                    'title': 'Петров модель земли под коралловым рифом',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                                   '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                                   '1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}
                   ]
    test_check_valid_return = test_check_valid.valid_issue_to_correct_colon_in_title(issues_json)

    return test_check_valid_return


@pytest.fixture
def supply_json_to_test_correct_colon_title_correct_input():
    """Make correct json for tests colons in title"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': 'иванов: модель кактуса',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n*Аннотация:* замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_colon_in_title(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_colon_description_incorrect_input():
    """Make incorrect json for tests colons in description"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
                   'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                                  '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                  '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                  '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                                  '1. Изучить\n'
                                  '1. Исследовать\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                  '1. Разработать\n'
                                  '1. Предоставить\n\n---\n'
                                  '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_colon_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_colon_description_incorrect_input_double_colon():
    """Make incorrect json for tests colons in description"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
                   'description': '**Тема курсовой работы семестра::** замените_Т_Е_М_У\n\n---\n'
                                  '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                  '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                  '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                                  '1. Изучить\n'
                                  '1. Исследовать\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                  '1. Разработать\n'
                                  '1. Предоставить\n\n---\n'
                                  '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_num_of_colon_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_colon_description_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests colons in description"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
                   'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                                  '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                  '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                  '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                                  '1. Изучить\n'
                                  '1. Исследовать\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                  '1. Разработать\n'
                                  '1. Предоставить\n\n---\n'
                                  '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
                   {'id': 32405282, 'iid': 3, 'project_id': 17692961,
                    'title': 'иванов: модель кактуса',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                                   '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                                   '1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
                   {'id': 32405282, 'iid': 4, 'project_id': 17692961,
                    'title': 'Петров: модель земли под коралловым рифом',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                                   '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                                   '1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}
                   ]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_colon_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_colon_description_correct_input():
    """Make correct json for tests colons in description"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
                   'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                                  '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                  '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                  '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                                  '1. Изучить\n'
                                  '1. Исследовать\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                  '1. Разработать\n'
                                  '1. Предоставить\n\n---\n'
                                  '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_colon_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_separator_in_kurs_incorrect_input():
    """Make incorrect json for tests separator in kurs"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n--\n'
         '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
         '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
         '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
         '1. Изучить\n'
         '1. Исследовать\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
         '1. Разработать\n'
         '1. Предоставить\n\n---\n'
         '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_kurs_to_correct_separator_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_separator_in_kurs_incorrect_input_four_separators():
    """Make incorrect json for tests separator in kurs"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n----\n'
         '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
         '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
         '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
         '1. Изучить\n'
         '1. Исследовать\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
         '1. Разработать\n'
         '1. Предоставить\n\n---\n'
         '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_kurs_to_correct_separator_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_separator_in_kurs_incorrect_input_without_separator():
    """Make incorrect json for tests separator in kurs"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель зе под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n'
         '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
         '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
         '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
         '1. Изучить\n'
         '1. Исследовать\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
         '1. Разработать\n'
         '1. Предоставить\n\n---\n'
         '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_kurs_to_correct_separator_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_separator_in_kurs_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests separator in kurs"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': 'иванов: модель кактуса',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n*Аннотация:* замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n--\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
                   {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
                   'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                                  '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                  '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                  '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                                  '1. Изучить\n'
                                  '1. Исследовать\n\n---\n'
                                  '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                  '1. Разработать\n'
                                  '1. Предоставить\n\n---\n'
                                  '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
                   {'id': 32405282, 'iid': 2, 'project_id': 17692961,
                    'title': 'Петров: модель земли под коралловым рифом',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                                   '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n--\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                                   '1. Изучить\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}
                   ]
    test_check_valid_return = test_check_valid.valid_issue_kurs_to_correct_separator_in_description(issues_json)

    return test_check_valid_return


@pytest.fixture
def supply_json_to_test_correct_separator_in_kurs_correct_input():
    """Make correct json for tests separator in kurs"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
         '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
         '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
         '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
         '1. Изучить\n'
         '1. Исследовать\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
         '1. Разработать\n'
         '1. Предоставить\n\n---\n'
         '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_kurs_to_correct_separator_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_separator_in_diplom_incorrect_input():
    """Make incorrect json for tests separator in diplom"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33397062, 'iid': 6, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
                    'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n--\n'
                    '**Аннотация:** Разработка программного средства формирования расписания из тематических '
                    'планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                    '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических планов '
                    'учебных дисциплин и пожеланий преподавателей.\n\n--\n'
                    '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                    '1. Аналитический обзор существующих алгоритмов и готовых решений для формирования расписания преподавателей.\n'
                    '1. Разработка архитектуры разрабатываемого программного средства формирования расписания.\n'
                    '1. Изучение формата файла тематического плана учебной дисциплины.\n'
                    '1. Реализация модуля для разбора файла тематического плана учебной дисциплины.\n'
                    '1. Анализ существующих особенностей расписания преподавателей *(например, день для филиала Квант должен быть выделен монопольно)*\n'
                    '1. Выработка формата для возможных пожеланий преподавателей '
                    '*(ставить преподавателя только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, '
                    'пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                    '1. Реализация модуля для разбора пожеланий преподавателей.\n'
                    '1. Реализация программного средства формирования расписания, позволяющего для заданного множества '
                    'тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---',}]
    test_check_valid_response = test_check_valid.valid_issue_diplom_to_correct_separator_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_separator_in_diplom_incorrect_input_without_separators():
    """Make incorrect json for tests separator in diplom"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33397062, 'iid': 6, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
                    'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n--\n'
                    '**Аннотация:** Разработка программного средства формирования расписания из тематических '
                    'планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                    '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических планов '
                    'учебных дисциплин и пожеланий преподавателей.\n\n-\n'
                    '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                    '1. Аналитический обзор существующих алгоритмов и готовых решений для формирования расписания преподавателей.\n'
                    '1. Разработка архитектуры разрабатываемого программного средства формирования расписания.\n'
                    '1. Изучение формата файла тематического плана учебной дисциплины.\n'
                    '1. Реализация модуля для разбора файла тематического плана учебной дисциплины.\n'
                    '1. Анализ существующих особенностей расписания преподавателей *(например, день для филиала Квант должен быть выделен монопольно)*\n'
                    '1. Выработка формата для возможных пожеланий преподавателей '
                    '*(ставить преподавателя только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, '
                    'пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                    '1. Реализация модуля для разбора пожеланий преподавателей.\n'
                    '1. Реализация программного средства формирования расписания, позволяющего для заданного множества '
                    'тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n',}]
    test_check_valid_response = test_check_valid.valid_issue_diplom_to_correct_separator_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_separator_in_diplom_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests separator in diplom"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33397062, 'iid': 6, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
                    'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n--\n'
                    '**Аннотация:** Разработка программного средства формирования расписания из тематических '
                    'планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                    '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических планов '
                    'учебных дисциплин и пожеланий преподавателей.\n\n--\n'
                    '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                    '1. Аналитический обзор существующих алгоритмов и готовых решений для формирования расписания преподавателей.\n'
                    '1. Разработка архитектуры разрабатываемого программного средства формирования расписания.\n'
                    '1. Изучение формата файла тематического плана учебной дисциплины.\n'
                    '1. Реализация модуля для разбора файла тематического плана учебной дисциплины.\n'
                    '1. Анализ существующих особенностей расписания преподавателей *(например, день для филиала Квант должен быть выделен монопольно)*\n'
                    '1. Выработка формата для возможных пожеланий преподавателей '
                    '*(ставить преподавателя только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, '
                    'пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                    '1. Реализация модуля для разбора пожеланий преподавателей.\n'
                    '1. Реализация программного средства формирования расписания, позволяющего для заданного множества '
                    'тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---',},
                   {'id': 33397062, 'iid': 5, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
                    'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n---\n'
                                   '**Аннотация:** Разработка программного средства формирования расписания из тематических '
                                   'планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                                   '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических планов '
                                   'учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                                   '1. Аналитический обзор существующих алгоритмов и готовых решений для формирования расписания преподавателей.\n'
                                   '1. Разработка архитектуры разрабатываемого программного средства формирования расписания.\n'
                                   '1. Изучение формата файла тематического плана учебной дисциплины.\n'
                                   '1. Реализация модуля для разбора файла тематического плана учебной дисциплины.\n'
                                   '1. Анализ существующих особенностей расписания преподавателей *(например, день для филиала Квант должен быть выделен монопольно)*\n'
                                   '1. Выработка формата для возможных пожеланий преподавателей '
                                   '*(ставить преподавателя только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, '
                                   'пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                                   '1. Реализация модуля для разбора пожеланий преподавателей.\n'
                                   '1. Реализация программного средства формирования расписания, позволяющего для заданного множества '
                                   'тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---', },
                   {'id': 33397062, 'iid': 4, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
                    'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n--\n'
                                   '**Аннотация:** Разработка программного средства формирования расписания из тематических '
                                   'планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                                   '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических планов '
                                   'учебных дисциплин и пожеланий преподавателей.\n\n--\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                                   '1. Аналитический обзор существующих алгоритмов и готовых решений для формирования расписания преподавателей.\n'
                                   '1. Разработка архитектуры разрабатываемого программного средства формирования расписания.\n'
                                   '1. Изучение формата файла тематического плана учебной дисциплины.\n'
                                   '1. Реализация модуля для разбора файла тематического плана учебной дисциплины.\n'
                                   '1. Анализ существующих особенностей расписания преподавателей *(например, день для филиала Квант должен быть выделен монопольно)*\n'
                                   '1. Выработка формата для возможных пожеланий преподавателей '
                                   '*(ставить преподавателя только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, '
                                   'пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                                   '1. Реализация модуля для разбора пожеланий преподавателей.\n'
                                   '1. Реализация программного средства формирования расписания, позволяющего для заданного множества '
                                   'тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---', }
                   ]
    test_check_valid_response = test_check_valid.valid_issue_diplom_to_correct_separator_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_separator_in_diplom_correct_input():
    """Make correct json for tests separator in diplom"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33397062, 'iid': 6, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
                    'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n---\n'
                    '**Аннотация:** Разработка программного средства формирования расписания из тематических '
                    'планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                    '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических планов '
                    'учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                    '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                    '1. Аналитический обзор существующих алгоритмов и готовых решений для формирования расписания преподавателей.\n'
                    '1. Разработка архитектуры разрабатываемого программного средства формирования расписания.\n'
                    '1. Изучение формата файла тематического плана учебной дисциплины.\n'
                    '1. Реализация модуля для разбора файла тематического плана учебной дисциплины.\n'
                    '1. Анализ существующих особенностей расписания преподавателей *(например, день для филиала Квант должен быть выделен монопольно)*\n'
                    '1. Выработка формата для возможных пожеланий преподавателей '
                    '*(ставить преподавателя только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, '
                    'пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                    '1. Реализация модуля для разбора пожеланий преподавателей.\n'
                    '1. Реализация программного средства формирования расписания, позволяющего для заданного множества '
                    'тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---',}]
    test_check_valid_response = test_check_valid.valid_issue_diplom_to_correct_separator_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_num_of_paragraph_kurs_spring_incorrect_input():
    """Make incorrect json for tests paragraphs in kurs spring"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_kurs_to_correct_paragraph_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_num_of_paragraph_kurs_spring_incorrect_input():
    """Make incorrect json for tests paragraphs in kurs spring"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_kurs_to_correct_paragraph_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_stars_incorrect_input():
    """Make incorrect json for tests paragraphs in kurs spring"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': 'Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Предоставить\n'
                        '1. Исследовать\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_stars_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_num_of_paragraph_kurs_spring_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests paragraphs in kurs spring"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Предоставить\n'
                        '1. Исследовать\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 3, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
         'description': '**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 4, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}
    ]
    test_check_valid_response = test_check_valid.valid_issue_kurs_to_correct_paragraph_in_description(issues_json)

    return test_check_valid_response



@pytest.fixture
def supply_json_to_test_num_of_paragraph_kurs_spring_correct_input():
    """Make correct json for tests paragraphs in kurs spring"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
         '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
         '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
         '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
         '1. Изучить\n'
         '1. Исследовать\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
         '1. Разработать\n'
         '1. Предоставить\n\n---\n'
         '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_kurs_to_correct_paragraph_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_stars_incorrect_input():
    """Make incorrect json for tests highlighting in description"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': 'Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Предоставить\n'
                        '1. Исследовать\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_stars_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_stars_incorrect_input_in_the_end():
    """Make incorrect json for tests highlighting in description"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель  под коралловым риф',
         'description': '**Тема курсовой работы семестра:  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Предоставить\n'
                        '1. Исследовать\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_stars_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_stars_incorrect_input_without_highlighting():
    """Make incorrect json for tests highlighting in description"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель  под риф',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        'Цель курсовой работы осеннего семестра: замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Предоставить\n'
                        '1. Исследовать\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_stars_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_num_of_paragraph_kurs_autumn_incorrect_input():
    """Make incorrect json for tests paragraphs in kurs autumn"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': 'иванов: модель кактуса',
                    'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                                   '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                                   '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                                   '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                                   '1. Исследовать\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                                   '1. Разработать\n'
                                   '1. Предоставить\n\n---\n'
                                   '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_kurs_to_correct_paragraph_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_num_of_paragraph_kurs_autumn_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests paragraphs in kurs autumn"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 3, 'project_id': 17692961, 'title': 'Пв: земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 4, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
         'description': '**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Предоставить\n'
                        '1. Исследовать\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}
    ]
    test_check_valid_response = test_check_valid.valid_issue_kurs_to_correct_paragraph_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_num_of_paragraph_kurs_autumn_correct_input():
    """Make correct json for tests paragraphs in kurs autumn"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
         '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
         '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
         '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
         '1. Изучить\n'
         '1. Исследовать\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
         '1. Разработать\n'
         '1. Предоставить\n\n---\n'
         '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_kurs_to_correct_paragraph_in_description(issues_json)

    return test_check_valid_response



@pytest.fixture
def supply_json_to_test_num_of_paragraph_diplom_incorrect_input():
    """Make incorrect json for tests paragraphs in diplom"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33397062, 'iid': 6, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
                      'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n--\n'
                      '**Аннотация:** Разработка программного средства формирования расписания из '
                      'тематических планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                      '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                      '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                      '1. Аналитический обзор существующих алгоритмов и готовых решений '
                      'для формирования расписания преподавателей.\n'
                      '1. Анализ существующих особенностей расписания преподавателей *(например, день для филиала Квант должен быть выделен монопольно)*\n'
                      '1. Выработка формата для возможных пожеланий преподавателей '
                      '*(ставить преподавателя только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, '
                      'пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                      '1. Реализация программного средства формирования расписания, '
                      'позволяющего для заданного множества тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---',}]
    test_check_valid_response = test_check_valid.valid_issue_diplom_to_correct_paragraph_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_num_of_paragraph_diplom_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests paragraphs in diplom"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33397062, 'iid': 6, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
                    'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n---\n'
                    '**Аннотация:** Разработка программного средства формирования расписания из тематических '
                    'планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                    '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических планов '
                    'учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                    '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                    '1. Аналитический обзор существующих алгоритмов и готовых решений для формирования расписания преподавателей.\n'
                    '1. Разработка архитектуры разрабатываемого программного средства формирования расписания.\n'
                    '1. Изучение формата файла тематического плана учебной дисциплины.\n'
                    '1. Реализация модуля для разбора файла тематического плана учебной дисциплины.\n'
                    '1. Анализ существующих особенностей расписания преподавателей *(например, день для филиала Квант должен быть выделен монопольно)*\n'
                    '1. Выработка формата для возможных пожеланий преподавателей '
                    '*(ставить преподавателя только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, '
                    'пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                    '1. Реализация модуля для разбора пожеланий преподавателей.\n'
                    '1. Реализация программного средства формирования расписания, позволяющего для заданного множества '
                    'тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---',},
                   {'id': 33397062, 'iid': 7, 'project_id': 17692961, 'title': 'иванов: модель кактуса',
                    'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n--\n'
                                   '**Аннотация:** Разработка программного средства формирования расписания из '
                                   'тематических планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                                   '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                                   '1. Аналитический обзор существующих алгоритмов и готовых решений '
                                   'для формирования расписания преподавателей.\n'
                                   '1. Анализ существующих особенностей расписания преподавателей *(например, день для филиала Квант должен быть выделен монопольно)*\n'
                                   '1. Выработка формата для возможных пожеланий преподавателей '
                                   '*(ставить преподавателя только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, '
                                   'пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                                   '1. Реализация программного средства формирования расписания, '
                                   'позволяющего для заданного множества тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---', },
                   {'id': 33397062, 'iid': 8, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
                    'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n--\n'
                                   '**Аннотация:** Разработка программного средства формирования расписания из '
                                   'тематических планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                                   '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                                   '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                                   '1. Аналитический обзор существующих алгоритмов и готовых решений '
                                   'для формирования расписания преподавателей.\n'
                                   '1. Анализ существующих особенностей расписания преподавателей *(например, день для филиала Квант должен быть выделен монопольно)*\n'
                                   '1. Выработка формата для возможных пожеланий преподавателей '
                                   '*(ставить преподавателя только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, '
                                   'пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                                   '1. Реализация программного средства формирования расписания, '
                                   'позволяющего для заданного множества тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---', }
                   ]
    test_check_valid_response = test_check_valid.valid_issue_diplom_to_correct_paragraph_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_num_of_paragraph_diplom_correct_input():
    """Make correct json for tests paragraphs in diplom"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33397062, 'iid': 6, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
                    'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n---\n'
                    '**Аннотация:** Разработка программного средства формирования расписания из тематических '
                    'планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                    '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических планов '
                    'учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                    '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                    '1. Аналитический обзор существующих алгоритмов и готовых решений для формирования расписания преподавателей.\n'
                    '1. Разработка архитектуры разрабатываемого программного средства формирования расписания.\n'
                    '1. Изучение формата файла тематического плана учебной дисциплины.\n'
                    '1. Реализация модуля для разбора файла тематического плана учебной дисциплины.\n'
                    '1. Анализ существующих особенностей расписания преподавателей *(например, день для филиала Квант должен быть выделен монопольно)*\n'
                    '1. Выработка формата для возможных пожеланий преподавателей '
                    '*(ставить преподавателя только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, '
                    'пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                    '1. Реализация модуля для разбора пожеланий преподавателей.\n'
                    '1. Реализация программного средства формирования расписания, позволяющего для заданного множества '
                    'тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---',}]
    test_check_valid_response = test_check_valid.valid_issue_diplom_to_correct_paragraph_in_description(issues_json)

    return test_check_valid_response



@pytest.fixture
def supply_json_to_test_correct_symbols_in_description_incorrect_input():
    """Make incorrect correct json for tests symbols in description"""
    test_check_valid = ValidationModule()

    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:**  замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_symbol_in_description_field(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_symbols_in_description_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests symbols in description"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 1, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
         '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
         '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
         '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
         '1. Изучить\n'
         '1. Исследовать\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
         '1. Разработать\n'
         '1. Предоставить\n\n---\n'
         '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 3, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
         'description': '**Тема курсовой работы семестра:** bnmlkknзамените_Т_Е_М_У\n\n---\n'
                        '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:**  замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}
    ]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_symbol_in_description_field(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_symbols_in_description_correct_input():
    """Make correct json for tests symbols in description"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
         '**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
         '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
         '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
         '1. Изучить\n'
         '1. Исследовать\n\n---\n'
         '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
         '1. Разработать\n'
         '1. Предоставить\n\n---\n'
         '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_symbol_in_description_field(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_whitespace_title_incorrect_input():
    """Make incorrect json for tests whitespace in title"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров:модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_whitespace_in_title(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_whitespace_title_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests whitespace in title"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 1, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров:модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 3, 'project_id': 17692961, 'title': 'Сопрано:если ли жизнь после докера',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}
    ]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_whitespace_in_title(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_whitespace_title_correct_input():
    """Make correct json for tests whitespace in title"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_whitespace_in_title(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_whitespace_theme_incorrect_input():
    """Make incorrect json for tests whitespace in theme"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров:  модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_whitespace_in_title(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_whitespace_theme_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests whitespace in theme"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 3, 'project_id': 17692961, 'title': 'Петров:  модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 4, 'project_id': 17692961, 'title': 'Сопрано:  если ли жизнь после докера',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}
    ]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_whitespace_in_title(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_whitespace_theme_correct_input():
    """Make correct json for tests whitespace in theme"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** kjh,замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_whitespace_in_title(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_whitespace_description_incorrect_input():
    """Make incorrect json for tests whitespace in description"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_whitespace_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_whitespace_description_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests whitespace in description"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 3, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 32405282, 'iid': 4, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
         'description': '**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}
    ]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_whitespace_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_whitespace_description_correct_input():
    """Make correct json for tests whitespace in description"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_whitespace_in_description(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_description_existance_correct_input():
    """Make correct json for tests description existance"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_description_existance(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_description_existance_incorrect_input():
    """Make incorrect json for tests description existance"""
    test_check_valid = ValidationModule()
    issues_json = [{'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': 'иванов: модель кактуса', 'description': ''}]
    test_check_valid_response = test_check_valid.valid_issue_to_description_existance(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_description_existance_incorrect_and_correct_input():
    """Make json with correct and incorrect issue for tests description existance"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n-\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'},
        {'id': 33117776, 'iid': 3, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом', 'description': ''},
        {'id': 33117776, 'iid': 4, 'project_id': 17692961, 'title': 'иванов: модель кактуса', 'description': ''}]
    test_check_valid_response = test_check_valid.valid_issue_to_description_existance(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_find_by_teachers_correct_input():
    """Make correct json for tests search by teachers"""
    issue_teachers = [{'id': 33397062, 'iid': 6, 'project_id': 17692961, 'title': 'Сопрано: если ли жизнь после докера',
                      'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n---\n'
                                     '**Аннотация:** Разработка программного средства формирования расписания из тематических планов учебных '
                                     'дисциплин и пожеланий преподавателей.\n\n---\n'
                                     '**Цель дипломной работы:** Разработка программного средства формирования расписания из тематических '
                                     'планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                                     '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                                     '1. Аналитический обзор существующих алгоритмов и готовых решений для формирования расписания преподавателей.\n'
                                     '1. Разработка архитектуры разрабатываемого программного средства формирования расписания.\n'
                                     '1. Изучение формата файла тематического плана учебной дисциплины.\n'
                                     '1. Реализация модуля для разбора файла тематического плана учебной дисциплины.\n'
                                     '1. Анализ существующих особенностей расписания преподавателей *(например, день для'
                                     ' филиала Квант должен быть выделен монопольно)*\n'
                                     '1. Выработка формата для возможных пожеланий преподавателей *(ставить преподавателя только в указанные дни, '
                                     'не ставить преподавателя в выбранные дни, учёт конференций, пожелания по первым/последним парам, субботам, '
                                     'максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                                     '1. Реализация модуля для разбора пожеланий преподавателей.\n'
                                     '1. Реализация программного средства формирования расписания, позволяющего для заданного множества '
                                     'тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---',},
                     {'id': 33397062, 'iid': 6, 'project_id': 17692961, 'title': 'Ильин: если ли жизнь после докера',
                      'description': '**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин.\n\n---\n'
                                     '**Аннотация:** Разработка программного средства формирования расписания из тематических '
                                     'планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                                     '**Цель дипломной работы:** Разработка программного средства формирования расписания из '
                                     'тематических планов учебных дисциплин и пожеланий преподавателей.\n\n---\n'
                                     '**Основные вопросы и задачи, подлежащие разработке при выполнении дипломной работы:**\n'
                                     '1. Аналитический обзор существующих алгоритмов и готовых решений для формирования расписания преподавателей.\n'
                                     '1. Разработка архитектуры разрабатываемого программного средства формирования расписания.\n'
                                     '1. Изучение формата файла тематического плана учебной дисциплины.\n'
                                     '1. Реализация модуля для разбора файла тематического плана учебной дисциплины.\n'
                                     '1. Анализ существующих особенностей расписания преподавателей *(например, день'
                                     ' для филиала Квант должен быть выделен монопольно)*\n'
                                     '1. Выработка формата для возможных пожеланий преподавателей *(ставить преподавателя '
                                     'только в указанные дни, не ставить преподавателя в выбранные дни, учёт конференций, пожелания по первым/последним парам, субботам, максимальному количеству занятий в день и разрывам пар первая-четвертая)*.\n'
                                     '1. Реализация модуля для разбора пожеланий преподавателей.\n'
                                     '1. Реализация программного средства формирования расписания, позволяющего для заданного '
                                     'множества тематических планов и пожеланий преподавателей формировать .xlsx-файл расписания.\n\n---', }]

    return issue_teachers


@pytest.fixture
def supply_json_to_test_correct_whitespace_after_description_incorrect_input():
    """Make correct json for tests whitespace after description field"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модель земли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_whitespaces_after_description_field(issues_json)

    return test_check_valid_response


@pytest.fixture
def supply_json_to_test_correct_whitespace_after_description_incorrect_input_over_enough():
    """Make correct json for tests whitespace after description field"""
    test_check_valid = ValidationModule()
    issues_json = [
        {'id': 32405282, 'iid': 2, 'project_id': 17692961, 'title': 'Петров: модельмли под коралловым рифом',
         'description': '**Тема курсовой работы семестра:** замените_Т_Е_М_У\n\n---\n'
                        '**Аннотация:** замените_Т_Е_К_С_Т_аннотации\n\n---\n'
                        '**Цель курсовой работы осеннего семестра:** замените_Ц_Е_Л_Ь_осеннего_семестра\n\n---\n'
                        '**Цель курсовой работы весеннего семестра:** замените_Ц_Е_Л_Ь_весеннего_семестра\n\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы осеннего семестра:**\n'
                        '1. Изучить\n'
                        '1. Исследовать\n\n---\n'
                        '**Основные вопросы и задачи, подлежащие разработке при выполнении курсовой работы весеннего семестра:**\n'
                        '1. Разработать\n'
                        '1. Предоставить\n\n---\n'
                        '**Рекомендации преподавателя:** ЗАМЕНЯТЬ_НЕОБЯЗАТЕЛЬНО\n\n---'}]
    test_check_valid_response = test_check_valid.valid_issue_to_correct_whitespaces_after_description_field(issues_json)

    return test_check_valid_response
