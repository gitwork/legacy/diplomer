# pylint: disable=C0301,C0305
"""Test correct colons in issue"""


def test_validation_module_to_correct_colon_in_title_incorrect_input(supply_json_to_test_correct_colon_title_incorrect_input):
    """Test incorrect colon in title"""
    answer = supply_json_to_test_correct_colon_title_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got colon in: иванов модель кактуса " \
                                                                                  "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][0]['id'] == 3


def test_validation_module_to_correct_colon_in_title_incorrect_input_double_colon(supply_json_to_test_correct_num_of_colon_in_title_incorrect_input_double_colon):
    """Test incorrect colon in title"""
    answer = supply_json_to_test_correct_num_of_colon_in_title_incorrect_input_double_colon
    assert str(answer['bad_issues'][0]['error']) == "Probably you have a lot of colon in: иван:: при " \
                                                                                  "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][0]['id'] == 3


def test_validation_module_to_correct_colon_in_title_incorrect_and_correct_input(supply_json_to_test_correct_colon_title_incorrect_and_correct_input):
    """Test incorrect and correct colon in title"""
    answer = supply_json_to_test_correct_colon_title_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got colon in: иванов модель кактуса " \
                                                                                  "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert str(answer['bad_issues'][1]['error']) == "Probably you haven`t got colon in: Петров модель земли под коралловым рифом " \
                                                                                  "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][0]['id'] == 3
    assert answer['bad_issues'][1]['id'] == 2


def test_validation_module_to_correct_colon_in_title_correct_input(supply_json_to_test_correct_colon_title_correct_input):
    """Test correct colon in title"""
    answer = supply_json_to_test_correct_colon_title_correct_input
    assert answer['bad_issues'] == []


def test_to_correct_colon_in_description_incorrect_input(supply_json_to_test_correct_colon_description_incorrect_input):
    """Test incorrect colon in description"""
    answer = supply_json_to_test_correct_colon_description_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got colon: \n" \
                                                                                    "**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n " \
                                                                                    "in issue: Петров: модель земли под коралловым рифом You should fill this " \
                                                                                    "field like this **Тема работы:** " \
                                                                                    "Программное средство формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 2


def test_to_correct_colon_in_description_incorrect_input_double_colon(supply_json_to_test_correct_colon_description_incorrect_input_double_colon):
    """Test incorrect colon in description"""
    answer = supply_json_to_test_correct_colon_description_incorrect_input_double_colon
    assert str(answer['bad_issues'][0]['error']) == "Probably you have got a lot of colon: **Тема курсовой работы семестра::** " \
                                                    "замените_Т_Е_М_У\n\n in issue: Петров: модель земли под коралловым рифом You should fill this " \
                                                    "field like this **Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 2


def test_to_correct_colon_in_description_correct_input(supply_json_to_test_correct_colon_description_correct_input):
    """Test correct colon in description"""
    answer = supply_json_to_test_correct_colon_description_correct_input
    assert answer['bad_issues'] == []


def test_to_correct_colon_in_description_incorrect_and_correct_input(supply_json_to_test_correct_colon_description_incorrect_and_correct_input):
    """Test incorrect and correct colon in description"""
    answer = supply_json_to_test_correct_colon_description_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got colon: \n" \
                                                                                    "**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n " \
                                                                                    "in issue: иванов: модель кактуса You should fill this " \
                                                                                    "field like this **Тема работы:** " \
                                                                                    "Программное средство формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 3
    assert str(answer['bad_issues'][1]['error']) == "Probably you haven`t got colon: \n" \
                                                    "**Аннотация** замените_Т_Е_К_С_Т_аннотации\n\n " \
                                                    "in issue: Петров: модель земли под коралловым рифом You should fill this " \
                                                    "field like this **Тема работы:** " \
                                                    "Программное средство формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][1]['id'] == 4


def test_validation_module_to_correct_colon_in_title_incorrect_input_one_word(supply_json_to_test_correct_colon_title_incorrect_input_one_word):
    """Test incorrect colon in title"""
    answer = supply_json_to_test_correct_colon_title_incorrect_input_one_word
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got colon in: ивановк " \
                                                                                  "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][0]['id'] == 3


def test_validation_module_to_correct_colon_in_title_incorrect_input_only_colon(supply_json_to_test_correct_colon_title_incorrect_input_only_colon):
    """Test incorrect colon in title"""
    answer = supply_json_to_test_correct_colon_title_incorrect_input_only_colon
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got colon in: : " \
                                                                                  "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][0]['id'] == 3


def test_validation_module_to_correct_colon_in_title_incorrect_input_startswith_no_letter(supply_json_to_test_correct_colon_title_incorrect_input_startswith_no_letter):
    """Test incorrect colon in title"""
    answer = supply_json_to_test_correct_colon_title_incorrect_input_startswith_no_letter
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got colon in: >4#>?: &; " \
                                                                                  "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][0]['id'] == 3
