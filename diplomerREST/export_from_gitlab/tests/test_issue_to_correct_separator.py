# pylint: disable=C0301
"""Test to correct separators in kurs and diplom"""


def test_to_correct_separator_in_kurs_incorrect_input(supply_json_to_test_correct_separator_in_kurs_incorrect_input):
    """Test kurs with incorrect separator"""
    answer = supply_json_to_test_correct_separator_in_kurs_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you have incorrect separators in issue: Петров: модель земли под коралловым рифом Correct separator is  ---"
    assert answer['bad_issues'][0]['id'] == 2


def test_to_correct_separator_in_kurs_incorrect_and_correct_input(supply_json_to_test_correct_separator_in_kurs_incorrect_and_correct_input):
    """Test kurs with incorrect and correct separator"""
    answer = supply_json_to_test_correct_separator_in_kurs_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you have incorrect separators in issue: иванов: модель кактуса Correct separator is  ---"
    assert answer['bad_issues'][0]['id'] == 3
    assert str(answer['bad_issues'][1]['error']) == "Probably you have incorrect separators in issue: Петров: модель земли под коралловым рифом Correct separator is  ---"
    assert answer['bad_issues'][1]['id'] == 2


def test_to_correct_separator_in_kurs_incorrect_input_four_separators(supply_json_to_test_correct_separator_in_kurs_incorrect_input_four_separators):
    """Test kurs with incorrect separator"""
    answer = supply_json_to_test_correct_separator_in_kurs_incorrect_input_four_separators
    assert str(answer['bad_issues'][0]['error']) == "Probably you have incorrect separators in issue: Петров: модель земли под коралловым рифом Correct separator is  ---"
    assert answer['bad_issues'][0]['id'] == 2


def test_to_correct_separator_in_kurs_correct_input(supply_json_to_test_correct_separator_in_kurs_correct_input):
    """Test kurs with correct separator"""
    answer = supply_json_to_test_correct_separator_in_kurs_correct_input
    assert answer['bad_issues'] == []


def test_to_correct_separator_in_diplom_incorrect_input(supply_json_to_test_correct_separator_in_diplom_incorrect_input):
    """Test diplom with incorrect separator"""
    answer = supply_json_to_test_correct_separator_in_diplom_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you have incorrect separators in issue: Сопрано: если ли жизнь после докера Correct separator is  ---"
    assert answer['bad_issues'][0]['id'] == 6


def test_to_correct_separator_in_diplom_incorrect_and_correct_input(supply_json_to_test_correct_separator_in_diplom_incorrect_and_correct_input):
    """Test diplom with incorrect and correct separator"""
    answer = supply_json_to_test_correct_separator_in_diplom_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you have incorrect separators in issue: Сопрано: если ли жизнь после докера Correct separator is  ---"
    assert answer['bad_issues'][0]['id'] == 6
    assert str(answer['bad_issues'][1]['error']) == "Probably you have incorrect separators in issue: Петров: модель земли под коралловым рифом Correct separator is  ---"
    assert answer['bad_issues'][1]['id'] == 4


def test_to_correct_separator_in_diplom_correct_input(supply_json_to_test_correct_separator_in_diplom_correct_input):
    """Test diplom with correct separator"""
    answer = supply_json_to_test_correct_separator_in_diplom_correct_input
    assert answer['bad_issues'] == []


def test_to_correct_separator_in_kurs_incorrect_input_without_separator(supply_json_to_test_correct_separator_in_kurs_incorrect_input_without_separator):
    """Test kurs with incorrect separator"""
    answer = supply_json_to_test_correct_separator_in_kurs_incorrect_input_without_separator
    assert str(answer['bad_issues'][0]['error']) == "Probably you have incorrect separators in issue: Петров: модель зе под коралловым рифом Correct separator is  ---"
    assert answer['bad_issues'][0]['id'] == 2


def test_to_correct_separator_in_diplom_incorrect_input_without_separators(supply_json_to_test_correct_separator_in_diplom_incorrect_input_without_separators):
    """Test diplom with incorrect separator"""
    answer = supply_json_to_test_correct_separator_in_diplom_incorrect_input_without_separators
    assert str(answer['bad_issues'][0]['error']) == "Probably you have incorrect separators in issue: Сопрано: если ли жизнь после докера Correct separator is  ---"
    assert answer['bad_issues'][0]['id'] == 6
