# pylint: disable=C0301
"""Test issue to correct whitespace"""


def test_issue_to_correct_whitespace_title_incorrect_input(supply_json_to_test_correct_whitespace_title_incorrect_input):
    """Test issue to correct whitespace in title with incorrect input"""
    answer = supply_json_to_test_correct_whitespace_title_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got whitespace in: Петров:модель земли под коралловым рифом " \
                                                                                    "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][0]['id'] == 2


def test_issue_to_correct_whitespace_title_incorrect_and_correct_input(supply_json_to_test_correct_whitespace_title_incorrect_and_correct_input):
    """Test issue to correct whitespace in title with incorrect and correct input"""
    answer = supply_json_to_test_correct_whitespace_title_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got whitespace in: Петров:модель земли под коралловым рифом " \
                                                                                    "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][0]['id'] == 2
    assert str(answer['bad_issues'][1]['error']) == "Probably you haven`t got whitespace in: Сопрано:если ли жизнь после докера " \
                                                                                    "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][1]['id'] == 3


def test_issue_to_correct_whitespace_title_correct_input(supply_json_to_test_correct_whitespace_title_correct_input):
    """Test issue to correct whitespace in title with correct input"""
    answer = supply_json_to_test_correct_whitespace_title_correct_input
    assert answer['bad_issues'] == []


def test_issue_to_correct_whitespace_theme_incorrect_input(supply_json_to_test_correct_whitespace_theme_incorrect_input):
    """Test issue to correct whitespace in theme with incorrect input"""
    answer = supply_json_to_test_correct_whitespace_theme_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you have got extra whitespace in:  модель земли под коралловым рифом " \
                                                                                    "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][0]['id'] == 2



def test_issue_to_correct_whitespace_theme_incorrect_and_correct_input(supply_json_to_test_correct_whitespace_theme_incorrect_and_correct_input):
    """Test issue to correct whitespace in theme with incorrect and correct input"""
    answer = supply_json_to_test_correct_whitespace_theme_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you have got extra whitespace in:  модель земли под коралловым рифом " \
                                                                                    "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][0]['id'] == 3
    assert str(answer['bad_issues'][1]['error']) == "Probably you have got extra whitespace in:  если ли жизнь после докера " \
                                                                                    "You should fill this field like this Петров: модель земли под коралловым рифом"
    assert answer['bad_issues'][1]['id'] == 4


def test_issue_to_correct_whitespace_theme_correct_input(supply_json_to_test_correct_whitespace_theme_correct_input):
    """Test issue to correct whitespace in theme with correct input"""
    answer = supply_json_to_test_correct_whitespace_theme_correct_input
    assert answer['bad_issues'] == []


def test_issue_to_correct_whitespace_description_incorrect_input(supply_json_to_test_correct_whitespace_description_incorrect_input):
    """Test issue to correct whitespace in description with incorrect input"""
    answer = supply_json_to_test_correct_whitespace_description_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you have got extra whitespace in: " \
                                                                                    "**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n " \
                                                                                    "in issue: Петров: модель земли под коралловым рифом You should fill this field like this " \
                                                                                    "**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 2


def test_issue_to_correct_whitespace_description_incorrect_and_correct_input(supply_json_to_test_correct_whitespace_description_incorrect_and_correct_input):
    """Test issue to correct whitespace in description with incorrect and correct input"""
    answer = supply_json_to_test_correct_whitespace_description_incorrect_and_correct_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you have got extra whitespace in: " \
                                                                                    "**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n " \
                                                                                    "in issue: Петров: модель земли под коралловым рифом You should fill this field like this " \
                                                                                    "**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 3
    assert str(answer['bad_issues'][1]['error']) == "Probably you have got extra whitespace in: " \
                                                                                    "**Тема курсовой работы семестра:**  замените_Т_Е_М_У\n\n " \
                                                                                    "in issue: Сопрано: если ли жизнь после докера You should fill this field like this " \
                                                                                    "**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][1]['id'] == 4


def test_issue_to_correct_whitespace_description_correct_input(supply_json_to_test_correct_whitespace_description_correct_input):
    """Test issue to correct whitespace in description with correct input"""
    answer = supply_json_to_test_correct_whitespace_description_correct_input
    assert answer['bad_issues'] == []


def test_issue_to_correct_whitespace_after_description_incorrect_input(supply_json_to_test_correct_whitespace_after_description_incorrect_input):
    """Test issue to correct whitespace after description field"""
    answer = supply_json_to_test_correct_whitespace_after_description_incorrect_input
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got enough whitespaces after description field: Петров: модель земли под коралловым рифом You should fill this field like this **Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 2


def test_issue_to_correct_whitespace_after_description_incorrect_input_over_enough(supply_json_to_test_correct_whitespace_after_description_incorrect_input_over_enough):
    """Test issue to correct whitespace after description field"""
    answer = supply_json_to_test_correct_whitespace_after_description_incorrect_input_over_enough
    assert str(answer['bad_issues'][0]['error']) == "Probably you haven`t got enough whitespaces after description field: Петров: " \
                                                    "модельмли под коралловым рифом You should fill this field like this " \
                                                    "**Тема работы:** Программное средство формирования расписания из тематических планов учебных дисциплин."
    assert answer['bad_issues'][0]['id'] == 2
