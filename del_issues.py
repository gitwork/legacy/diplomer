#!/usr/bin/env python3
import sys
import gitlab


PROJECT_ID = 3778  # ID if https://gitwork.ru/sub/kidp

def main():
    gl = gitlab.Gitlab('https://gitwork.ru', private_token=sys.argv[1])
    gl.auth()

    project = gl.projects.get(PROJECT_ID)

    #issues = project.issues.list(all=True, state='closed')
    issues = project.issues.list(all=True)
    for issue in issues:
        #print(dir(issue))
        #print(issue)
        print(issue.title, end='')
        res = input("\t Пустой ввод для пропуска, любое сообщение для удаления: ")
        if res:
            print('Удаляем...')
            issue.delete()
        else:
            print('Пропускаем')

if __name__ == "__main__":
    main()
