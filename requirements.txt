Django==3.0.4
djangorestframework==3.11.0
pytest==5.4.1
pytest-django==3.9.0
requests==2.23.0
urllib3==1.25.8
